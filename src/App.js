
// imports
import React  from "react";
import $ from 'jquery';
import { Animated } from "react-animated-css";
import { isMobile } from "react-device-detect";

import 'normalize.css';
import "./animate.css";
import "./App.scss";

import top from './static/top.png'
import middle from './static/middle.png'
import base from './static/base.png'

import addr1 from './static/addr/line1.png'
import addr2 from './static/addr/line2.png'
import addr3 from './static/addr/line3.png'
import addr4 from './static/addr/line4.png'
import header from './static/header.png'

import scrollup from './static/scroll-up.png'


class App extends React.Component {

    constructor(props) {
        super(props);

        // loop middle items
        this.mids = [];
        this.height = isMobile ? 3000 : 1000;
        Array.from(Array(this.height), () =>
            this.mids.push(
                <img
                    key={Math.random(1)}
                    className="middle"
                    src={middle}
                    alt="SpringLeaf"
                />
            )
        );

        // state
        this.state = {
            visible: false,
            scrollup: false,
            header: false,
            line1: false,
            line2: false,
            line3: false,
            line4: false,
        }
    }

    // scroll page to buttom on load
    componentDidMount = () => {

        let _this = this;

        // scroll to bottom
        setTimeout(() => window.scrollTo(0, document.body.scrollHeight), 500);
        setTimeout(() => this.setState({ visible: true}), 550);

        $(window).on("scroll", function() {
            var scrollHeight = $(document).height();
            var scrollPosition = $(window).height() + $(window).scrollTop();
            
            // scroll percentage
            let scrollPercentage = scrollPosition / scrollHeight * 100;
            
            // scroll top logo visibility
            _this.setState({ scrollup: scrollPercentage > 98 ? true : false });
            // header
            _this.setState({ header: scrollPercentage > 75 ? false : true });
            // line1
            _this.setState({ line1: scrollPercentage > 50 ? false : true });
            // line2
            _this.setState({ line2: scrollPercentage > 30 ? false : true });
            // line3
            _this.setState({ line3: scrollPercentage > 15 ? false : true });
            // line 4
            _this.setState({ line4: scrollPercentage > 10 ? false : true });
        });
    }

    render() {

        // render
        return (
            <div className={`wrapper hidden ${this.state.visible ? 'visible' : ''}`}>
                <Animated
                    animationIn="fadeIn"
                    animationOut="fadeOut"
                    isVisible={this.state.scrollup}
                    animationInDuration={300}
                    animationOutDuration={300}>
                    <div className="scroll-container">
                        <img src={scrollup} className="scroll" alt="Scroll Up"/>
                    </div>
                </Animated>
                
                <div className="logo-container">
                    <Animated
                        animationIn="fadeIn"
                        animationOut="fadeOut"
                        isVisible={this.state.header}
                        animationInDuration={300}
                        animationOutDuration={300}>                        
                            <img src={header} className="header"/>
                    </Animated>
                    <Animated
                        animationIn="fadeIn"
                        animationOut="fadeOut"
                        isVisible={this.state.line1}
                        animationInDuration={300}
                        animationOutDuration={300}>                        
                            <img src={addr1} className="addr"/>
                    </Animated>
                    <Animated
                        animationIn="fadeIn"
                        animationOut="fadeOut"
                        isVisible={this.state.line2}
                        animationInDuration={300}
                        animationOutDuration={300}>                        
                            <img src={addr2} className="addr"/>
                    </Animated>
                    <Animated
                        animationIn="fadeIn"
                        animationOut="fadeOut"
                        isVisible={this.state.line3}
                        animationInDuration={300}
                        animationOutDuration={300}>                        
                            <img src={addr3} className="addr"/>
                    </Animated>
                    <Animated
                        animationIn="fadeIn"
                        animationOut="fadeOut"
                        isVisible={this.state.line4}
                        animationInDuration={300}
                        animationOutDuration={300}>                        
                            <img src={addr4} className="addr"/>
                    </Animated>
                </div>
                <div className={`building-container`}>
                    <div className="cloud-container"></div>
                    <div className="building-inner">
                        <img src={top} className="top" alt="SpringLeaf"/>
                        { this.mids }
                        <img className="base" src={base} alt="SpringLeaf"/>
                    </div>
                    <div className="overlay"></div>
                </div>
            </div>
        );
    }
}

//
export default App;